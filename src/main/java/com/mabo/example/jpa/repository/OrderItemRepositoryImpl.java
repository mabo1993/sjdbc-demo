/*
 * Copyright 1999-2015 dangdang.com.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 */

package com.mabo.example.jpa.repository;

import com.mabo.example.jpa.entity.OrderItem;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class OrderItemRepositoryImpl implements OrderItemRepository {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public OrderItem selectById(final long orderItemId) {
        return entityManager.find(OrderItem.class, orderItemId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<OrderItem> selectAll() {
        return (List<OrderItem>) entityManager.createQuery("SELECT o FROM OrderItem o").getResultList();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<OrderItem> selectOrderBy() {
        return (List<OrderItem>) entityManager.createQuery("SELECT o FROM OrderItem o order by o.itemId").getResultList();
    }
    
    @Override
    public void create(final OrderItem orderItem) {
        entityManager.persist(orderItem);
    }
    
    @Override
    public void update(final OrderItem orderItem) {
        Query query = entityManager.createQuery("UPDATE OrderItem o SET o.itemId = ?1 WHERE o.orderId = ?2 AND o.userId = ?3");
        query.setParameter(1, orderItem.getItemId());
        query.setParameter(2, orderItem.getOrderId());
        query.setParameter(3, orderItem.getUserId());
        query.executeUpdate();
    }
    
    @Override
    public void delete(final long orderId) {
        Query query = entityManager.createQuery("DELETE FROM OrderItem o WHERE o.itemId = ?1 AND o.userId = 51");
        query.setParameter(1, orderId);
        query.executeUpdate();
    }

    @Override
    public List<OrderItem> selectPageOne() {
        return (List<OrderItem>) entityManager.createNativeQuery("SELECT * from t_order_item ORDER BY item_id DESC ").getResultList();
    }

    @Override
    public List<OrderItem> selectPageTwo() {
        return (List<OrderItem>) entityManager.createNativeQuery("SELECT * from t_order_item ORDER BY item_id LIMIT 5,5").getResultList();
    }

    @Override
    public List<Object[]> selectOrderByDate() {
        return (List<Object[]>) entityManager.createNativeQuery("select * from tb_config  order by UPDATETIME desc ").getResultList();
    }
}
