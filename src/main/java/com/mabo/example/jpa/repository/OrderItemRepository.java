/*
 * Copyright 1999-2015 dangdang.com.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 */

package com.mabo.example.jpa.repository;

import com.mabo.example.jpa.entity.OrderItem;

import java.util.List;

public interface OrderItemRepository {
    
    OrderItem selectById(long itemId);
    
    List<OrderItem> selectAll();
    
    List<OrderItem> selectOrderBy();
    
    void create(OrderItem order);
    
    void update(OrderItem order);
    
    void delete(long itemId);

    List<OrderItem> selectPageOne();

    List<OrderItem> selectPageTwo();

    List<Object[]> selectOrderByDate();
}
