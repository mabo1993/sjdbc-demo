/*
 * Copyright 1999-2015 dangdang.com.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 */

package com.mabo.example.jpa;

import com.mabo.example.jpa.entity.OrderItem;
import com.mabo.example.jpa.repository.OrderItemRepository;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

// CHECKSTYLE:OFF
public final class Main {
    
    public static void main(final String[] args) {
        // CHECKSTYLE:ON
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("META-INF/jpa/mysql/jpaContext.xml");
        OrderItemRepository orderItemRepository = applicationContext.getBean(OrderItemRepository.class);
//        List<OrderItem> list1 = orderItemRepository.selectPageOne();
//        List<OrderItem> list2 =  orderItemRepository.selectPageTwo();
//        System.out.println(list1.size());
//        System.out.println(list2.size());
        List<Object[]> results = orderItemRepository.selectOrderByDate();
        System.out.println(results.size());
        applicationContext.close();
    }
}
